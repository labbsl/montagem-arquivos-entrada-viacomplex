//*******************************************************************************
// Universidade Estadual Paulista Julio de Mesquita Filho
// Instituto de Biociencias
// Campus de Botucatu
// BSL - Laboratorio de Estudos em Biocomplexidade
// Autor: André Luiz Molan
// Orientador: Prof. Dr. José Luiz Rybarczyk Filho
// Software para preparação de arquivos de entrada do Viacomplex baseado no arquivo de GOs (BP e MF) baixados via string.
// Este programa filtra as GOs com score de potência inferior a -3 e faz a ligação com os arquivos que contém os genes diferencialmente
// expressos.
// Data da criação: 24/08/2015
// Última modifação: 25/08/2015
//*******************************************************************************
#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<dirent.h> //Varrer pasta para abertura de arquivos
#include<sys/stat.h> //Criar pasta para armazenamento de novos arquivos
//***************
//Declaração das funções secundárias a serem utilizadas. O propósito de cada uma está descrito detalhadamente no
//cabeçalho de cada uma deles logo após a função main
//***************
void gene_go(char *cNomArq1, char *id_rede, char *id_procs);
void gene_exp(char *cNomArq1, char *id_rede);
//***************
//Definição de estruturas
//***************
struct ontology{
       char go_name[20]; //nome da GO
       char id_gene[20]; //identificação dos genes relacionados a GO (uma GO pode ter um ou mais genes)
       char id_netw[20]; //nome da rede na qual a GO foi identificada via STRING (S1,S2, ...)
       char id_proc[20]; //processo a que a GO está relacionada (BP = processos biológicos; MF = funções moleculares)
       
}GO[100000]; //Estrutura para armazenar os dados das ontologias (BP e MF)

struct arquivo{
       char arq_name[100];//nome do arquivo
       char arq_path[300];//caminho para o arquivo
       char id_proc[20];//identificação do tipo do arquivo (BP = processos biológicos, MF = funções moleculares) => APENAS PARA ARQ_GO[]
       char id_netw[20]; //nome da rede (S1,S2, ...)
}ARQ_GO[1000],ARQ_EXP[1000]; //ARQ_GO = arquivos de GOs, ARQ_EXP = arquivos de expressao

struct expressao{
       char id_gene[20];//nome do gene
       char id_netw[20];//nome da rede (S1,S2, ...)
       char condition[20];
}EXP[100000];
//***************
//Definição de variáveis globais
//***************
int iContGen=0;
int iContOnt=0;
//***************
// Função principal
//***************
main()
//***************
{
  char cOrigem_1[300];//Caminho para os arquivos de GOs (BP e MF)
  char cOrigem_2[300];//Caminho para os arquivos contendo os dados de expressao
  char cNomArq1[]="parametros.txt"; //Arquivo de parâmetros (caminho para busca dos arquivos de GOs e dados de expressão)
  char nome_arq[50];
  char nome_arq1[]="score.txt";
  int iContArqGo=0, iContArqExp=0;
  int i=0, j=0, k=0, l=0, m=0, iFind=0, iFind1=0;
  char auxiliar[300],auxiliar1[20],auxiliar2[300],auxiliar3[300];
  char *point1,*point2;

  FILE *dados1,*dados2,*dados3;

  DIR *dir;

  struct dirent *lsdir;

  //Abertura do arquivo de parâmetros
  dados1=fopen(cNomArq1,"r");
  if(dados1==NULL){
  	 printf("Arquivo de parametros nao encontrado. Impossivel prosseguir. Verifique !!!");
  	 getchar();
  	 exit(0);
    }
  else{
       fscanf(dados1,"%s",cOrigem_1);
       fscanf(dados1,"%s",cOrigem_2);
      }

  //Localização dos arquivos BP e MF
  dir = opendir(cOrigem_1);
  while((lsdir=readdir(dir))!=NULL)
       {
         if(strcmp(lsdir->d_name,".")!=0 && strcmp(lsdir->d_name,"..")!=0 && strstr(lsdir->d_name,".txt")!=NULL && strstr(lsdir->d_name,"_0")!=NULL && strstr(lsdir->d_name,"~")==NULL){
            strcpy(ARQ_GO[iContArqGo].arq_name,lsdir->d_name);
            strcpy(ARQ_GO[iContArqGo].arq_path,cOrigem_1);
            strcat(ARQ_GO[iContArqGo].arq_path,lsdir->d_name);
            strcpy(auxiliar,lsdir->d_name);
            point1=strtok(auxiliar,"_");
            strcpy(ARQ_GO[iContArqGo].id_netw,point1);
            point1=strtok(NULL,"_");
            point1=strtok(NULL,"_");
            point2=strtok(point1,".");
            strcpy(ARQ_GO[iContArqGo].id_proc,point2);
            gene_go(ARQ_GO[iContArqGo].arq_path,ARQ_GO[iContArqGo].id_netw,ARQ_GO[iContArqGo].id_proc);
            iContArqGo++;
           }
       }
  closedir(dir);

  //Localização dos arquivos de expressao
  dir = opendir(cOrigem_2);
  while((lsdir=readdir(dir))!=NULL)
       {
         if(strcmp(lsdir->d_name,".")!=0 && strcmp(lsdir->d_name,"..")!=0 && strstr(lsdir->d_name,".txt")!=NULL && strstr(lsdir->d_name,"EXP")!=NULL && strstr(lsdir->d_name,"~")==NULL){
            strcpy(ARQ_EXP[iContArqExp].arq_name,lsdir->d_name);
            strcpy(ARQ_EXP[iContArqExp].arq_path,cOrigem_2);
            strcat(ARQ_EXP[iContArqExp].arq_path,lsdir->d_name);
            strcpy(auxiliar,lsdir->d_name);
            point1=strtok(auxiliar,"_");
            strcpy(ARQ_EXP[iContArqExp].id_netw,point1);
            gene_exp(ARQ_EXP[iContArqExp].arq_path,ARQ_EXP[iContArqExp].id_netw); //extração dos genes dos arquivos de expressão
            iContArqExp++;
           }
       }
  closedir(dir);

 //Impressão dos arquivos de relacionamento entre GOs e Genes expressos 
 dados3=fopen(nome_arq1,"w");
 while(i<iContOnt){
      
      for(j=0;j<=iContGen;j++){
          strcpy(EXP[j].condition,"0.0");
         }
      
      j=0;   
      while(strcmp(GO[i].id_netw,EXP[j].id_netw)!=0 && j<=iContGen!=0){
            j++;
           }
      k=j;
      strcpy(auxiliar,GO[i].go_name);
      strcpy(auxiliar1,GO[i].id_netw);
      strcpy(auxiliar2,GO[i].id_proc);
       
      iFind=0;
      l=0;
      while(iFind==0 && i<iContOnt){
            iFind1=0;
            j=k;
            while(iFind1==0 && j<=iContGen){
                  if(strcmp(EXP[j].id_gene,GO[i].id_gene)==0 && strcmp(EXP[j].id_netw,auxiliar1)==0){
                     strcpy(EXP[j].condition,"1.0");
                     l++;
                     iFind=1;
                    }
                  j++;
                 }
             i++;
             iFind=strcmp(auxiliar,GO[i].go_name);  
           }

      if(l>0){
         strcpy(nome_arq,"");
         strcat(nome_arq,auxiliar1);
         strcat(nome_arq,"_");
         strcat(nome_arq,auxiliar2);
         strcat(nome_arq,"_");
         point1=strtok(auxiliar,":");
         strcat(nome_arq,point1);
         point1=strtok(NULL,":");
         strcat(nome_arq,point1);
         strcat(nome_arq,".txt");

         fprintf(dados3,"%-30s %d\n",nome_arq,l);

         dados2=fopen(nome_arq,"w");

         fprintf(dados2,"N	ID_Type	Condition\n");

         m=1;
         for(j=k;j<=iContGen;j++){
             if(strcmp(EXP[j].id_netw,auxiliar1)==0){
                fprintf(dados2,"%d %s %s\n",m,EXP[j].id_gene,EXP[j].condition);
                m++;
               }
            }

         fflush(dados2);
         fclose(dados2);
       }
     }

  fflush(dados3);
  fclose(dados3);
  fclose(dados1);
}
//*************************
//Função para extrair os genes dos arquivos de GOs
//*************************
void gene_go(char *cNomArq1, char *id_rede, char *id_procs)
//*************************
{
 int i=0, j=0, iFim=0;
 int score=3;
 char auxiliar[500];
 char auxiliar1[500];
 char compare1[]="#";
 char compare2[]="E-";  
 char *point1, *point2;

 FILE *dados1;

 dados1=fopen(cNomArq1,"r");

 if(dados1==NULL){ 
    printf("Arquivo %s de ontologia nao encontrado. Verifique!!!",cNomArq1);
    getchar();
    exit(0);
   }

 while(!feof(dados1) && iFim==0){
       if(i==0){
          point1=NULL;
          while(point1==NULL && !feof(dados1)){
                fscanf(dados1,"%s",auxiliar);
                point1=strstr(auxiliar,compare1);
               }
          i++;
         }

       if(j==0){
          fscanf(dados1,"%s",auxiliar1);
         }

       point1=NULL;

       while(point1==NULL){
             fscanf(dados1,"%s",auxiliar);
             point1=strstr(auxiliar,compare2);
            }

       point2=strtok(auxiliar,"-");
       point2=strtok(NULL,"-");  
       
       if(atoi(point2)>=score){
          fscanf(dados1,"%s",auxiliar);
          fscanf(dados1,"%s",auxiliar);
          point1=NULL;
          while(point1==NULL){
                fscanf(dados1,"%s",auxiliar);
                point1=strstr(auxiliar,"GO:"); 
                if(point1==NULL){
                   strcpy(GO[iContOnt].go_name,auxiliar1);
                   strcpy(GO[iContOnt].id_gene,auxiliar);
                   strcpy(GO[iContOnt].id_netw,id_rede);
                   strcpy(GO[iContOnt].id_proc,id_procs);
                   iContOnt++;
                  }
                else{
                     strcpy(auxiliar1,auxiliar);
                     j=1;
                    } 
               }
         }
       else{
            iFim=1;
           }
      }

 fclose(dados1);
}
//*************************
//Função para extrair os genes dos arquivos de expressao
//*************************
void gene_exp(char *cNomArq1, char *id_rede)
//*************************
{
 int i=0, j=0;
 char auxiliar[30]; 

 FILE *dados1;

 dados1=fopen(cNomArq1,"r");
 if(dados1==NULL){ 
    printf("Arquivo %s de expressao nao encontrado. Verifique!!!",cNomArq1);
    getchar();
    exit(0);
   }

 while(!feof(dados1)){
       if(i==0){ 
          fscanf(dados1,"%s",auxiliar);
          fscanf(dados1,"%s",auxiliar);
          fscanf(dados1,"%s",auxiliar);
          fscanf(dados1,"%s",auxiliar);
          i++;
         }
       fscanf(dados1,"%s",EXP[iContGen].id_gene);
       strcpy(EXP[iContGen].id_netw,id_rede);
       fscanf(dados1,"%s",auxiliar);
       fscanf(dados1,"%s",auxiliar);
       fscanf(dados1,"%s",auxiliar);
       iContGen++;
      }
 iContGen--; 
}
//*************************
//FIM DO CÓDIGO DO PROGRAMA
//*************************